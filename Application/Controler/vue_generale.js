var vue = 1;
document.getElementById("vueGlobale").style.display = 'none';
document.getElementById("vuePage").style.display = 'none';

function changeView(){
  if(vue === 1){
    document.getElementById("vueGlobale").style.display = 'none';
    document.getElementById("vuePage").style.display = 'block';
  } else{
    document.getElementById("vuePage").style.display = 'none';
    document.getElementById("vueGlobale").style.display = 'block';
    render();
  }
  vue = 1-vue;
}

function vueAccueil(){
  document.getElementById("vueAccueil").style.display = 'none';
  document.getElementById("vueGlobale").style.display = 'block';
  render();
}

function recadrer(event) {
  // Si la barre espace est pressée et la vue globale est ouverte
  if (event.keyCode === 32 && document.getElementById("vueGlobale").style.display === 'block') {
    // Remise à zéro du background et du zoom
    background.x = 0;
    background.y = 0;
    zoom = 0.7;
    render();
  }
}

function fullScreenSelect(id) {
  // Changement de l'icône
  var img = document.getElementById(id);
  var source = img.src;
  source = source.substr(-5);

  if (source === "n.svg") {
    img.src = "../Vue/data/images_svg/full_screen_exit.svg";
  } else {
    img.src = "../Vue/data/images_svg/full_screen.svg";
  }
}

// Pour la mise de l'interface en plein écran
function fullScreen(){
  if(!estFull){
    document.getElementsByTagName('body')[0].requestFullscreen();
    estFull = true;
  }else{
    document.exitFullscreen();
    estFull = false;
  }
}
