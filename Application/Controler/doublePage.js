var cHeight = window.innerHeight-(window.innerHeight*0.30);
var cWidth = cHeight*(5/7);

// On regarde si la largeur des 2 canvas est trop grande selon la fenêtre
if ((cWidth*2) > (window.innerWidth-(window.innerWidth*0.25))) {
  // la largeur correspond aux 3/4 de la fenêtre divisé par 2 (car on a 2 canvas)
  cWidth = (window.innerWidth-(window.innerWidth*0.25))/2;
  cHeight = cWidth*(7/5);
}
var ratioRetourX = 1/(cWidth/(idealWidth));
var ratioRetourY = 1/(cHeight/(idealHeight));

var canvasGauche = new fabric.Canvas('pageGauche');
var canvasDroit = new fabric.Canvas('pageDroite');
canvasGauche.backgroundColor = "white";


var pageAfficher;

/**
* Permet d'ajouter des éléments à un canva en correspondance avec une page du modèle
* @param listElem  Liste des éléments de la page à afficher
* @param canva Canva dans lequel afficher ces éléments
* @param idPageAAfficher La page à afficher
*/
function chargeElemPage(listElem,canva,idPageAAfficher){

  // En respectant le ratio de la taille de l'album lorsque l'on crée les canvas,
  // nous n'avons pas besoin de 2 ratios
  let ratioX = (canva.width/(idealWidth));
  let ratioY = (canva.height/(idealHeight));
  //let ratio = (idealWidth>idealHeight ? idealHeight/idealWidth : idealWidth/idealHeight);

  for(var i = 0; i < listElem.length; i++){
    if('chaine' in listElem[i]) {
      // Correspond à un texte
      let text = new fabric.IText((listElem[i].chaine===null ? "    " : listElem[i].chaine),
      {
        idE : i,
        idP : idPageAAfficher,
        left: listElem[i].position.posX*ratioX,
        top: listElem[i].position.posY*ratioY,
        fontSize: listElem[i].taillePolice * ratioY,
        fontFamily: (listElem[i].font !== null ? listElem[i].font : "arial"),
        fontWeight: (listElem[i].bold ? 'bold' : null),
        fontStyle: (listElem[i].italic ? 'italic' : 'normal'),
        textAlign: listElem[i].align,
        underline: listElem[i].underline,
        angle:  listElem[i].rotation,
        scaleX: listElem[i].echelleX,
        scaleY: listElem[i].echelleY,
        fill: (listElem[i].couleurPolice!=="default"?listElem[i].couleurPolice:"black")
      });
      canva.add(text);
      text.on('modified',function () {
        Enregistrement(text, canva);
      });
    } else {
      if(listElem[i].source !== ""){
        // Si l'image a une source, alors on ajoute une vraie image
        fabric.Image.fromURL(
          listElem[i].source,
          function(img){
            canva.add(img);
            img.on('modified',function () {
              Enregistrement(img, canva);
            });
          }, {
            "idE": i,
            "idP": idPageAAfficher,
            "left": listElem[i].position.posX*ratioX,
            "top": listElem[i].position.posY*ratioY,
            "angle": listElem[i].rotation,
            "scaleX": listElem[i].echelleX,
            "scaleY": listElem[i].echelleY
        });
      } else {
        // Si l'image n'a pas de source, alors on ajoute un carré unicolore
        canva.add(new fabric.Rect({
          "idE": i,
          "idP": idPageAAfficher,
          "left": listElem[i].position.posX*ratioX,
          "top": listElem[i].position.posY*ratioY,
          "fill": 'red',
          "angle": listElem[i].rotation,
          "height": listElem[i].taille.longueur*ratioY,
          "width": listElem[i].taille.largeur*ratioX,
          "scaleX": listElem[i].echelleX,
          "scaleY": listElem[i].echelleY
        }));
      }
    }
  }
}



function chargeElemPageM(listElem,canva,idPageAAfficher){

  // En respectant le ratio de la taille de l'album lorsque l'on crée les canvas,
  // nous n'avons pas besoin de 2 ratios
  let ratioX = (canva.width/(idealWidth));
  let ratioY = (canva.height/(idealHeight));
  //let ratio = (idealWidth>idealHeight ? idealHeight/idealWidth : idealWidth/idealHeight);

  for(var i = 0; i < listElem.length; i++){
    if('chaine' in listElem[i]) {
      // Correspond à un texte
      let text = new fabric.IText((listElem[i].chaine===null ? "    " : listElem[i].chaine),
      {
        idE : i,
        idP : idPageAAfficher,
        left: listElem[i].position.posX*ratioX,
        top: listElem[i].position.posY*ratioY,
        fontSize: listElem[i].taillePolice * ratioY,
        fontFamily: (listElem[i].font !== null ? listElem[i].font : "arial"),
        fontWeight: (listElem[i].bold ? 'bold' : null),
        fontStyle: (listElem[i].italic ? 'italic' : 'normal'),
        textAlign: listElem[i].align,
        underline: listElem[i].underline,
        angle:  listElem[i].rotation,
        scaleX: listElem[i].echelleX,
        scaleY: listElem[i].echelleY,
        fill: (listElem[i].couleurPolice!=="default"?listElem[i].couleurPolice:"black")
      });
      canva.add(text);
      text.on('modified',function () {
        Enregistrement(text, canva);
      });
    } else {
      if(listElem[i].source !== ""){
        // Si l'image a une source, alors on ajoute une vraie image
        fabric.Image.fromURL(
          listElem[i].source,
          function(img){
            canva.add(img);
            img.on('modified',function () {
              Enregistrement(img, canva);
            });
          }, {
            "idE": i,
            "idP": idPageAAfficher,
            "left": listElem[i].position.posX*ratioX,
            "top": listElem[i].position.posY*ratioY,
            // "height": listElem[i].taille.longueur*ratioY,
            // "width": listElem[i].taille.largeur*ratioX,
            "angle": listElem[i].rotation,
            "scaleX": listElem[i].echelleX*ratioX,
            "scaleY": listElem[i].echelleY*ratioY
        });
      } else {
        // Si l'image n'a pas de source, alors on ajoute un carré unicolore
        canva.add(new fabric.Rect({
          "idE": i,
          "idP": idPageAAfficher,
          "left": listElem[i].position.posX*ratioX,
          "top": listElem[i].position.posY*ratioY,
          "fill": 'red',
          "angle": listElem[i].rotation,
          "height": listElem[i].taille.longueur*ratioY,
          "width": listElem[i].taille.largeur*ratioX,
          "scaleX": listElem[i].echelleX,
          "scaleY": listElem[i].echelleY
        }));
      }
    }
  }
}

function afficherCadena(canva) {
  fabric.Image.fromURL(
    "../Vue/data/images_png/Cadenas.png",
    function(img){
      canva.add(img);
      canva.item(0).selectable = false;
    }, {
      "left": (canva.width/2) - 128,
      "top": (canva.height/2) - 128
  });
}

/**
*
* @param canva
* @param urlImg
*/
function ajoutBackGround(canva,urlImg="blanck") {
  if(urlImg==="blanck"){
    canva.backgroundColor="white";
    canva.backgroundOpacity=1;
    afficherCadena(canva);
    canva.renderAll();
  } else {
    canva.setBackgroundImage(urlImg, canva.renderAll.bind(canva), {
      backgroundImageOpacity: 0.5,
      backgroundImageStretch: true
    });
  }
}

function afficherPages(idPageAff = 1){
  let lesPages = document.getElementById("lesPages");
  if(idPageAff === 1) {
    lesPages.childNodes[3].style.display = 'none';
  }else{
    lesPages.childNodes[3].style.display = 'block';
  }

  // Mise à jour de la page affichée
  pageAfficher = idPageAff;

  // Vidage des canvas :
  canvasGauche.clear();
  canvasDroit.clear();

  if(idPageAff!==1){
    if(album.pageAtPos(idPageAff-1).fondDePage != null && idPageAff!==3){
      ajoutBackGround(canvasGauche,album.pageAtPos(idPageAff-1).fondDePage)
    } else if(idPageAff===3){
      ajoutBackGround(canvasGauche)
      canvasDroit.backgroundColor = "white";
      canvasDroit.renderAll.bind(canvasDroit);
      canvasDroit.backgroundOpacity = 0.5;
    } else {
      canvasGauche.backgroundColor = "white";
      canvasGauche.renderAll.bind(canvasGauche);
      canvasGauche.backgroundOpacity = 0.5;

      canvasDroit.backgroundColor = "white";
      canvasDroit.renderAll.bind(canvasDroit);
      canvasDroit.backgroundOpacity = 0.5;
    }
    if(album.pageAtPos(idPageAff).fondDePage != null){
      ajoutBackGround(canvasDroit,album.pageAtPos(idPageAff).fondDePage);
    }

    chargeElemPage(album.pageAtPos(idPageAff-1).elements,canvasGauche,idPageAff-1);
    chargeElemPage(album.pageAtPos(idPageAff).elements,canvasDroit,idPageAff);
  } else {
    if(album.pageAtPos(idPageAff).fondDePage != null){
      ajoutBackGround(canvasGauche,album.pageAtPos(idPageAff).fondDePage)
    } else {
      canvasGauche.backgroundColor = "white";
      canvasGauche.renderAll.bind(canvasGauche);
      canvasGauche.backgroundOpacity = 0.5;
    }
    chargeElemPage(album.pageAtPos(idPageAff).elements,canvasGauche,idPageAff);

  }

  // Affichage des numéros des pages dans le paragraphe situé en dessous de la double page
  var p = document.getElementById("numPage");
  if (idPageAff === 1) {
    p.innerHTML = "couverture";
  } else if (idPageAff === 3) {
    p.innerHTML = "1";
  } else if (idPageAff === album.nbPages-1) {
    p.innerHTML = (idPageAff-3)+" - 4ème de couv.";
  } else {
    p.innerHTML = (idPageAff-3)+" - "+(idPageAff-2);
  }
}

resize();

function resize(){
  canvasDroit.setHeight(cHeight);
  canvasDroit.setWidth(cWidth);
  canvasGauche.setHeight(cHeight);
  canvasGauche.setWidth(cWidth);
}

function Enregistrement(elem, canva){ // add update menu
  // Ratios permettant de retourner à l'échelle de l'album

  // Enregistrement des modifications dans le modèle
  if((elem.top < 0 && elem.height < Math.abs(elem.top))
  ||(elem.left < 0 && elem.width < Math.abs(elem.left))
  ||((elem.top > canva.height) || (elem.left > canva.width))){
    // Si l'élément est hors du canva, on le supprime
    //TODO : ENLEVER LES CONSOLES LOG
    //console.log(JSON.stringify(album.pageAtPos(elem.idP).elements[elements.length-1]));
    album.pageAtPos(elem.idP).supprimerElement(elem.idE);
    //console.log(JSON.stringify(album.pageAtPos(elem.idP).elements[elements.length-1]));
    canva.remove(elem);
  } else {
    album.pageAtPos(elem.idP).getElement(elem.idE).deplacer((elem.left*ratioRetourX),(elem.top*ratioRetourY));
  }

  // Si c'est un texte
  if('chaine' in album.pageAtPos(elem.idP).getElement(elem.idE)) {
    album.pageAtPos(elem.idP).getElement(elem.idE).chaine = elem.text;
  }

  album.pageAtPos(elem.idP).getElement(elem.idE).rotation = elem.angle;

  album.pageAtPos(elem.idP).getElement(elem.idE).changerTaille(elem.width,elem.height);
  album.pageAtPos(elem.idP).getElement(elem.idE).changerEchelle(elem.scaleX, elem.scaleY);

}

function suppression(){
  if(event.keyCode === 46){
    album.pageAtPos(elem.idP).getElement(elem.idE).remove();
  }
}

// Permet de passer d'une page à l'autre grâce aux flèches
function pagePrecedente() {
  if (pageAfficher !== 1) {
    afficherPages(pageAfficher-2);
  } else {
    alert("Vous êtes déjà sur la couverture");
  }
}

function pageSuivante() {
  if (pageAfficher !== album.nbPages-1) {
    afficherPages(pageAfficher+2);
  } else {
    alert("Vous êtes déjà sur la 4ème de couverture");
  }
}
