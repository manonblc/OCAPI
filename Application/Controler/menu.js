var menuOpen = "";
var boutonOpen = "";
var estFull = false;

function showMenu(menu, bouton){
  if(menuOpen != menu) { // On vérifie s'il est déja ouvert ou non
    if(menuOpen != "") {
      document.getElementById(menuOpen).style.width = "0";

      // On met le bouton en désélectionné
      var img = document.getElementById(boutonOpen);

      if (boutonOpen == 'menuH' || boutonOpen == 'menuH2') {
        img.src = "../Vue/data/images_svg/menu.svg";
      } else if (boutonOpen == 'helpI' || boutonOpen == 'aideI') {
        img.src = "../Vue/data/images_svg/help.svg";
      } else if (boutonOpen == 'libraryButton' || boutonOpen == 'photos') {
        img.src = "../Vue/data/images_svg/library.svg";
      } else if (boutonOpen == 'pages') {
        img.src = "../Vue/data/images_svg/pages.svg";
      } else if (boutonOpen == 'miseEnPage') {
        img.src = "../Vue/data/images_svg/layout.svg";
      } else if (boutonOpen == 'theme') {
        img.src = "../Vue/data/images_svg/paint.svg";
      } else if (boutonOpen == 'illust') {
        img.src = "../Vue/data/images_svg/sticker.svg";
      } else if (boutonOpen == 'textes'){
        img.src = "../Vue/data/images_svg/text.svg";
      }

      if(menu == "sidenav"){
        document.getElementsByClassName("contenu")[0].style.marginRight = "250px";
      }
    }

    // Ouverture du menu
    document.getElementById(menu).style.width = "250px";

    // On met le bouton en sélectionné
    var img = document.getElementById(bouton);

    if (bouton == 'menuH' || bouton == 'menuH2') {
      img.src = "../Vue/data/images_svg/menu_selected.svg";
    } else if (bouton == 'helpI' || bouton == 'aideI') {
      img.src = "../Vue/data/images_svg/help_selected.svg";
    } else if (bouton == 'libraryButton' || bouton == 'photos') {
      img.src = "../Vue/data/images_svg/library_selected.svg";
    } else if (bouton == 'pages') {
      img.src = "../Vue/data/images_svg/pages_selected.svg";
    } else if (bouton == 'miseEnPage') {
      img.src = "../Vue/data/images_svg/layout_selected.svg";
    } else if (bouton == 'theme') {
      img.src = "../Vue/data/images_svg/paint_selected.svg";
    } else if (bouton == 'illust') {
      img.src = "../Vue/data/images_svg/sticker_selected.svg";
    } else if (bouton == 'textes'){
      img.src = "../Vue/data/images_svg/text_selected.svg";
    }

    if(menu != "sidenav"){
      document.getElementsByClassName("contenu")[0].style.marginRight = "500px";
    }

    boutonOpen = bouton;
    menuOpen = menu;
  } else if (menuOpen == menu){
    // Si le menu est déja ouvert, on le ferme
    document.getElementById(menu).style.width = "0";
    if(menu != "sidenav"){
      document.getElementsByClassName("contenu")[0].style.marginRight = "250px";
    }

    // On met le bouton en désélectionné
    img = document.getElementById(bouton);

    if (bouton == 'menuH' || bouton == 'menuH2') {
      img.src = "../Vue/data/images_svg/menu.svg";
    } else if (bouton == 'helpI' || bouton == 'aideI') {
      img.src = "../Vue/data/images_svg/help.svg";
    } else if (bouton == 'libraryButton' || bouton == 'photos') {
      img.src = "../Vue/data/images_svg/library.svg";
    } else if (bouton == 'pages') {
      img.src = "../Vue/data/images_svg/pages.svg";
    } else if (bouton == 'miseEnPage') {
      img.src = "../Vue/data/images_svg/layout.svg";
    } else if (bouton == 'theme') {
      img.src = "../Vue/data/images_svg/paint.svg";
    } else if (bouton == 'illust') {
      img.src = "../Vue/data/images_svg/sticker.svg";
    } else if (bouton == 'textes'){
      img.src = "../Vue/data/images_svg/text.svg";
    }

    boutonOpen = "";
    menuOpen = "";
  }
}

// Correspond au menu de la banque d'images (les photos)
function libraryMenu(id){
  photos = getGallery();
  // On fait en sorte que les icones ne puissent pas être glissables (draggable)
  for (var i = 0; i < document.images.length; i++) {
    document.images[i].draggable = 0;
  }
  var img;
  var library = document.getElementById(id);

  // Clear de la galerie
  while (library.firstChild) {
    library.removeChild(library.firstChild);
  }

  // Ajout du titre
  var titre = document.createElement("h3");
  titre.innerHTML = "Vos photos";
  library.appendChild(titre);

  var parcourir = document.createElement("p");
  parcourir.className = "bouton";
  parcourir.innerHTML = "Parcourir";
  parcourir.title  = "Ajoutez des photos depuis votre ordinateur";

  library.appendChild(parcourir);
  // Creation des images
  photos.forEach(function(photo) {
    img = document.createElement("img");
    img.src = photo;
    img.className = "photos";
    // Fonctionnalité de drag & drop
    img.ondragstart= function(event){drag(event);};
    library.appendChild(img);
  });
  // Drop sur les canvas :ondrop="drop(event)" ondragover="allowDrop(event)"
  document.getElementsByClassName("canvas-container")[0].ondrop=function(event){drop(event)};
  document.getElementsByClassName("canvas-container")[0].ondragover=function(event){allowDrop(event)};
  document.getElementsByClassName("canvas-container")[1].ondrop=function(event){drop(event)};
  document.getElementsByClassName("canvas-container")[1].ondragover=function(event){allowDrop(event)};

  document.getElementById("bubulles").ondrop=function(event){dropGlobal(event)};
  document.getElementById("bubulles").ondragover=function(event){allowDrop(event)};
}

// Ajout d'une image au canvas et enregistrement de celle-ci dans le modèle
function addImg(target, src){
  let c;
  let ipage;
  // Si c'est la page n°1
  if(target.parentNode === document.getElementsByClassName("pages")[0].childNodes[1]){
    c = canvasGauche;
    ipage = pageAfficher-1;
  } else {
    c = canvasDroit;
    ipage = pageAfficher;
  }

  if(pageAfficher == 1) {
    ipage = pageAfficher;
  }

  album.nouvelElementAPage(ipage,new Position(0,0),"img");
  album.pageAtPos(ipage).elements[album.pageAtPos(ipage).elements.length-1].source = src;
  let imgBuffer = new Image();
  imgBuffer.src = src;
  album.pageAtPos(ipage).elements[album.pageAtPos(ipage).elements.length-1].changerTaille(imgBuffer.naturalWidth,imgBuffer.naturalHeight);

  fabric.Image.fromURL(
    src,
    function(img){
      c.add(img);
      Enregistrement(img, c);
      var scaleImg = (img.width > img.height ? (c.width/img.width):(c.height/img.height));
      img.set({scaleX : scaleImg,scaleY : scaleImg});
      album.pageAtPos(ipage).elements[album.pageAtPos(ipage).elements.length-1].echelleX = scaleImg;
      album.pageAtPos(ipage).elements[album.pageAtPos(ipage).elements.length-1].echelleY = scaleImg;
      img.on('modified',function () {
        Enregistrement(img, c);
      });
    }, {
      "idE": album.pageAtPos((target.parentNode == document.getElementsByClassName("pages")[0].childNodes[1] ?  pageAfficher-1: pageAfficher )).elements.length-1,
      "idP": (target.parentNode == document.getElementsByClassName("pages")[0].childNodes[1] ?  pageAfficher-1: pageAfficher )
    });
}

// Les 2 méthodes suivantes permettent de drag & drop une image (glisser/déposer)
function drag(ev = 0) {
  ev.dataTransfer.setData("text", ev.target.src);
}

function drop(ev) {
  ev.preventDefault();
  addImg(ev.target, ev.dataTransfer.getData("text"));
}
function getPageGlobal(x,y){
  let i = 0;
  let dx;
  let dy;
  let d2;
  let node;
  let cible = null;

  while(i < graph.nodes.length && cible===null){
    node = graph.nodes[i];

    dx = x - (node.x + background.x) * zoom;
    dy = y - (node.y + background.y) * zoom;
    d2 = dx * dx + dy * dy;

    if(node.id%2!==0){
      if(d2 < Math.pow(tailleBulle,2)){
        cible = node;
        if(node.id === 1 || node.id === 3){
          return node.id;
        } else {
          if(x <((node.x + background.x) * zoom)){
            return node.id-1;
          }else{
            return node.id;
          }
        }
      }
    }
    i = i + 1;
  }
}

function allowDrop(ev) {
  ev.preventDefault();
}

function dropGlobal(event) {
  event.preventDefault();
  id = getPageGlobal(event.clientX, event.clientY);
if(id !== undefined){
  let imgBuffer = new Image();
  imgBuffer.src = event.dataTransfer.getData("text");
  album.nouvelElementAPage(id,new Position(0,0),"img");
  album.pageAtPos(id).elements[album.pageAtPos(id).elements.length-1].changerTaille(imgBuffer.naturalWidth,imgBuffer.naturalHeight);
  //Calcule et ajout de l'échelle à l'image
  var scaleImg = (imgBuffer.naturalWidth > imgBuffer.naturalHeight ? (canvasGauche.width/imgBuffer.naturalWidth):(canvasGauche.height/imgBuffer.naturalHeight));
  album.pageAtPos(id).elements[album.pageAtPos(id).elements.length-1].echelleX = scaleImg;
  album.pageAtPos(id).elements[album.pageAtPos(id).elements.length-1].echelleY = scaleImg;
  //Ajout de la source de l'image
  album.pageAtPos(id).elements[album.pageAtPos(id).elements.length-1].source = event.dataTransfer.getData("text");

  render();
}
}
// Menu d'affichage de toutes les pages
var pageMenuHeight = 140;

function allPages(){
  // Clear
  var myNode = document.getElementById("allPages");
  while (myNode.firstChild) {
    myNode.removeChild(myNode.firstChild);
  }

  // Ajout du titre
  var titre = document.createElement("h3");
  titre.innerHTML = "Toutes les pages";
  myNode.appendChild(titre);

  // Creation des pages en canvas
  for(i = 1; i<=(album.nbPages); i+=2) {
    createPage(i);
  }
}

// Création de l'affichage de la double page pour le menu à pois
function createPage(i){
  var page = document.createElement("div");
  var text = document.createElement("p");

  var p = document.getElementById("numPage");
  if (i === 1) {
    text.innerHTML = "couverture";
  } else if (i === 3) {
    text.innerHTML = "1";
  } else if (i === album.nbPages-1) {
    text.innerHTML = (i-3)+" - 4ème de couv.";
  } else {
    text.innerHTML = (i-3)+" - "+(i-2);
  }

  page.addEventListener("click",function() { afficherPages(i);}, false);
  if(i != 1){
    var canvasGauche = document.createElement("canvas");
    canvasGauche.id='page'+(i-1);
    canvasGauche.width  = pageMenuHeight*(5/7);
    canvasGauche.height = pageMenuHeight;
    canvasGauche.style.backgroundColor = "white";
    page.appendChild(text);
    page.appendChild(canvasGauche);

    var canvasDroit = document.createElement("canvas");
    canvasDroit.id='page'+(i);
    canvasDroit.width  = pageMenuHeight*(5/7);
    canvasDroit.height = pageMenuHeight;
    canvasDroit.style.backgroundColor = "white";

    page.appendChild(canvasDroit);
  } else {
    var canvasGauche = document.createElement("canvas");
    canvasGauche.id='page'+(i);
    canvasGauche.width  = pageMenuHeight*(5/7);
    canvasGauche.height = pageMenuHeight;
    canvasGauche.style.backgroundColor = "white";

    page.appendChild(text);
    page.appendChild(canvasGauche);
  }

  document.getElementById('allPages').appendChild(page);
  if(i != 1){
    var fabricGauche = this.__canvas = new fabric.StaticCanvas('page'+(i-1));
    if(album.pageAtPos(i-1).fondDePage!= null){
      ajoutBackGround(fabricGauche,album.pageAtPos(i-1).fondDePage);
    }
    chargeElemPageM(album.pageAtPos(i-1).elements,fabricGauche,i-1);


    var fabricDroit = this.__canvas = new fabric.StaticCanvas('page'+(i));
    if(album.pageAtPos(i).fondDePage!= null){
      ajoutBackGround(fabricDroit,album.pageAtPos(i).fondDePage);
    }
    chargeElemPageM(album.pageAtPos(i).elements,fabricDroit,i);
  } else {
    var fabricGauche = this.__canvas = new fabric.StaticCanvas('page'+i);
    if(album.pageAtPos(i).fondDePage!= null){
      ajoutBackGround(fabricGauche,album.pageAtPos(i).fondDePage);
    }
    chargeElemPageM(album.pageAtPos(i).elements,fabricGauche,i);
  }
}


//  Dans le menu pour ajouter du texte (dernière bulle) :
//  Permet de modifier les images des boutons selon s'il est sélectionné ou non

function grasSelect(event) {
  var img = document.getElementById("gras");
  var source = img.src;
  source = source.substr(-6);

  if (source == "ld.svg") {
    img.src = "../Vue/data/images_svg/bold_selected.svg";
  } else {
    img.src = "../Vue/data/images_svg/bold.svg";
  }
}

function italiqueSelect() {
  var img = document.getElementById("italique");
  var source = img.src;
  source = source.substr(-5);

  if (source == "c.svg") {
    img.src = "../Vue/data/images_svg/italic_selected.svg";
  } else {
    img.src = "../Vue/data/images_svg/italic.svg";
  }
}

function soulignerSelect() {
  var img = document.getElementById("souligne");
  var source = img.src;
  source = source.substr(-5);

  if (source == "e.svg") {
    img.src = "../Vue/data/images_svg/underline_selected.svg";
  } else {
    img.src = "../Vue/data/images_svg/underline.svg";
  }
}

function textGaucheSelect() {
  var img = document.getElementById("textGauche");
  var source = img.src;
  source = source.substr(-5);

  if (source == "t.svg") {
    img.src = "../Vue/data/images_svg/align_left_selected.svg";
  }

  // On désélectionne les autres alignements
  document.getElementById("textCentre").src = "../Vue/data/images_svg/align_center.svg";
  document.getElementById("textDroite").src = "../Vue/data/images_svg/align_right.svg";

}

function textCentreSelect() {
  var img = document.getElementById("textCentre");
  var source = img.src;
  source = source.substr(-5);

  if (source == "r.svg") {
    img.src = "../Vue/data/images_svg/align_center_selected.svg";
  }

  // On désélectionne les autres alignements
  document.getElementById("textGauche").src = "../Vue/data/images_svg/align_left.svg";
  document.getElementById("textDroite").src = "../Vue/data/images_svg/align_right.svg";

}

function textDroiteSelect() {
  var img = document.getElementById("textDroite");
  var source = img.src;
  source = source.substr(-5);

  if (source == "t.svg") {
    img.src = "../Vue/data/images_svg/align_right_selected.svg";
  }

  // On désélectionne les autres alignements
  document.getElementById("textCentre").src = "../Vue/data/images_svg/align_center.svg";
  document.getElementById("textGauche").src = "../Vue/data/images_svg/align_left.svg";
}


// Permet d'ajouter les textes sur la page de gauche
function ajoutTexteG() {
  if (pageAfficher !== 3) {
    album.nouvelElementAPage(pageAfficher-1,new Position(10,10),"text");
    var elements = album.pageAtPos(pageAfficher-1).elements;
    var i = elements.length-1;

    ajoutTexte(elements, i);
    chargerTexteG(elements[i], i);
  } else alert("Cette page n'est pas modifiable");

}

// Permet d'ajouter les textes sur la page de droite
function ajoutTexteD() {
  album.nouvelElementAPage(pageAfficher,new Position(10,10),"text");
  var elements = album.pageAtPos(pageAfficher).elements;
  var i = elements.length-1;

  ajoutTexte(elements, i);
  chargerTexteD(elements[i], i);
}


// Permet de créer et modifier le texte selon les choix sélectionnés

function ajoutTexte(elements, i) {
  elements[i].chaine = "Modifiez votre texte";

  // On récupère la police et on modifie le texte avec cette police
  var formulaire = document.forms["ajoutText"];
  var index = formulaire.elements["taille"].selectedIndex;
  elements[i].taillePolice = formulaire.elements["taille"].options[index].value;

  // On regarde si l'option de texte en gras était sélectionnée
  var img = document.getElementById("gras");
  var source = img.src;
  source = source.substr(-6);

  if (source == "ed.svg") {
    elements[i].bold = true;
  }

  // On regarde si l'option de texte en italique était sélectionnée
  img = document.getElementById("italique");
  source = img.src;
  source = source.substr(-5);

  if (source == "d.svg") {
    elements[i].italic = true;
  }

  // On regarde si l'option de texte en souligné était sélectionnée
  img = document.getElementById("souligne");
  source = img.src;
  source = source.substr(-5);

  if (source == "d.svg") {
    elements[i].underline = true;
  }

  // On regarde l'alignement choisi
  img = document.getElementById("textGauche");
  source = img.src;
  source = source.substr(-5);

  // Vérifie l'alignement à gauche
  if (source == "d.svg") {
    elements[i].align = 'left';
  } else {
    img = document.getElementById("textCentre");
    source = img.src;
    source = source.substr(-5);

    // Vérifie l'alignement centré
    if (source == "d.svg") {
      elements[i].align = 'center';
    } else {
      // Sinon il s'agit de l'alignement à droite
      elements[i].align = 'right';
    }
  }

  // On regarde la police choisie
  index = formulaire.elements["police"].selectedIndex;
  elements[i].font = formulaire.elements["police"].options[index].value;
}


// Permet d'ajouter le texte sur la page de gauche
function chargerTexteG(element, i) {
  let ratioX = (canvasGauche.width/(idealWidth));  //En respectant le ratio de la taille de l'album lorsque l'on crée les canva on a pas besoin de 2 ratio
  let ratioY = (canvasGauche.height/(idealHeight));

  let text = new fabric.IText((element.chaine===null ? "    " : element.chaine),
    {
      idE : i,
      idP : pageAfficher,
      left: element.position.posX*ratioX,
      top: element.position.posY*ratioY,
      fontSize: element.taillePolice * ratioY,
      fontFamily: (element.font !== null ? element.font : "arial"),
      fontWeight: (element.bold ? 'bold' : null),
      fontStyle: (element.italic ? 'italic' : 'normal'),
      textAlign: element.align,
      underline: element.underline,
      angle:  element.rotation,
      scaleX: element.echelleX,
      scaleY: element.echelleY,
      fill: (element.couleurPolice!="default"?element.couleurPolice:"black")
    });
  canvasGauche.add(text);
  text.on('modified',function () {
    Enregistrement(text, canvasGauche);
  });
}



// Permet d'ajouter le texte sur la page de gauche
function chargerTexteD(element, i) {
  // En respectant le ratio de la taille de l'album lorsque l'on crée les canvas,
  // nous n'avons pas besoin de 2 ratios
  let ratioX = (canvasDroit.width/(idealWidth));
  let ratioY = (canvasDroit.height/(idealHeight));

  let text = new fabric.IText((element.chaine===null ? "    " : element.chaine),
    {
      idE : i,
      idP : pageAfficher,
      left: element.position.posX*ratioX,
      top: element.position.posY*ratioY,
      fontSize: element.taillePolice * ratioY,
      fontFamily: (element.font !== null ? element.font : "arial"),
      fontWeight: (element.bold ? 'bold' : null),
      fontStyle: (element.italic ? 'italic' : 'normal'),
      textAlign: element.align,
      underline: element.underline,
      angle:  element.rotation,
      scaleX: element.echelleX,
      scaleY: element.echelleY,
      fill: (element.couleurPolice!="default"?element.couleurPolice:"black")
    });
  canvasDroit.add(text);
  text.on('modified',function () {
    Enregistrement(text, canvasDroit);
  });
}
