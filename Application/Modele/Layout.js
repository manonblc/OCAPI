/**
* Représente un layout de page
* @class
*/
class Layout{
  /**
  * Crée un layout
  * @constructor
  */
  constructor(){
    this.emplacement = [];
  }

  get emplacement(){
    return this._emplacement;
  }
  
  set emplacement(nouvelEmplacement){
    this._emplacement = nouvelEmplacement;
  }

  ajouterEmplacement(nouvelleElement){
    this.emplacement.push(nouvelleElement);
  }
}
