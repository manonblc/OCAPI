/**
 * Contient toute la logique derrière le ctrl-Z et ctrl-Y
 */
 class Zevent {
   /**
   * Crée un Zevent
   * @constructor
   */
   constructor(){
     this._unDo = [];
     this._reDo = [];
   }

   get unDo() {
     return this._unDo;
   }

   set unDo(value) {
     this._unDo = value;
   }

   get reDo() {
     return this._reDo;
   }

   set reDo(value) {
     this._reDo = value;
   }

   /**
   * Applique l'action passée en paramètre sur l'album également passé en paramètre
   * @param {Action} action
   * @param {AlbumPhoto} album
   */
   faire(action,album){
     switch (action.type) {
       case add:
       if(exist){
         if(action.objet==="elem"){
           album.addElementAPage(action.idPage,new Position(10,10),action.complements[0]);
         } else {
           album.insertPage(action.idPage,action.complements[0]);
         }
       } else {
         if(action.objet==="elem"){
           album.nouvelElementAPage(action.idPage,new Position(10,10),action.complements[0]);
         } else {
           album.newPage(action.idPage);
         }
       }
       break;
       case del:
       if(action.objet==="elem"){
         album.pageAtPos(action.idPage).supprimerElement(action.complements[1]);
       } else {
         album.supprimerPage(action.idPage);
       }
       break;
       case modif:
       break;
       default:
       console.log("Erreur action type non reconnu !");
     }
   }

   /**
   * Renvoie un object action contraire à l'action passée en paramètres
   * @param {Action} action
   * @returns {Action}
   */
   actionContraire(action){
     var complements;
     var actionCont;
     if(action.typeAct==="add"){
       complements = [action.complements[0],action.complements[1]];
       actionCont = new Action("del",action.objet,action.exist,action.idPage,complements);
     } else {
       complements = [action.complements[0]];
       actionCont = new Action("add",action.objet,action.exist,action.idPage,complements);
     }
     return actionCont;
   }

   nouvelleAction(action){
     this.unDo.push(action);
     while(this.unDo.length > 15){
       this.unDo.shift();
     }
   }

   /**
   * Appel à faire sur l'action la plus récente et ajoute dans la pile des redo l'action contraire
   */
   retourArriere(){
     var action = this.unDo.pop();
     if (!(typeof action === 'undefined' || action === null)) {
       this.faire(action);
       this.unDo.pop();
       this.reDo.push(this.actionContraire(action));
     }
   }
 }
