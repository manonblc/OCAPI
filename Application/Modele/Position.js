/**
* Représente la position des éléments
* @class
*/
class Position{
  /**
  * Crée une position
  * @constructor
  * @param {number} x - La position sur l'axe X que l'on veut donner à position
  * @param {number} y - La largeur que l'axe Y que l'on veut donner à position
  */
  constructor(x, y){
    this.posX = x;
    this.posY = y;
  }

  set posX(x){
    this._posX = x;
  }

  set posY(y){
    this._posY = y;
  }

  get posX(){
    return this._posX;
  }

  get posY(){
    return this._posY;
  }
}
