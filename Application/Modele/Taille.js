/**
* Représente la taille des pages, de l'album et des éléments
* @class
*/
class Taille{
  /**
  * Représente la taille des pages, de l'album et des éléments
  * @constructor
  * @param {number} longueur - La longueur que l'on veut donner à taille
  * @param {number} largeur - La largeur que l'on veut donner à taille
  */
  constructor(longueur, largeur){
    this.longueur = longueur;
    this.largeur = largeur;
  }
  
  set longueur(nouvelleLongueur){
    this._longueur = nouvelleLongueur;
  }

  set largeur(nouvelleLargeur){
    this._largeur = nouvelleLargeur;
  }

  get longueur(){
    return this._longueur;
  }

  get largeur(){
    return this._largeur;
  }
}
