/**
* Représente un thème général
* @class
*/
class ThemeGeneral{
  /**
  * Crée un élément
  * @constructor
  * @param intitule - L'intitulé du thème
  * @param listeDeFond
  */
  constructor(intitule, listeDeFond = null){
    this.intitule = intitule;
    this.fonds = [];
    if(listeDeFond!=null){
      this.initFond(listeDeFond);
    }
  }

  get intitule(){
    return this._intitule;
  }

  get randFond(){
    return this.fonds[Math.floor(Math.random() * Math.floor(this.fonds.length))][0];
  }

  randCoupleFond(){
    return this.fonds[Math.floor(Math.random() * Math.floor(this.fonds.length))];
  }
  
  fondCorrespondantAID(idPage){
    return this.fonds[(idPage%(this.fonds.length))];
  }

  get fonds(){
    return this._fonds;
  }

  set fonds(nouveauFonds){
    this._fonds = nouveauFonds;
  }

  set intitule(nouvelleIntitule){
    this._intitule = nouvelleIntitule;
  }

  initFond(listeFonds){
    this.fonds = [];
    var j = 0;
    for (var i = 0; i < listeFonds.length; i+=2) {
      this.fonds[j] = [listeFonds[i],listeFonds[i+1]];
      j++;
    }
  }

}
