/**
* Représente une page
* @class
*/
class Page {
  /**
  * Crée une page
  * @constructor
  * @param {Position} position La position de la page dans l'album
  */
  constructor(position) {
    this.position = position;
    this.elements = [];
    this.fondDePage = null;
    this.layout = null;
    this.taille = null;
  }

  get position(){
    return this._position;
  }

  get elements(){
    return this._elements;
  }

  get layout(){
    return this._layout;
  }

  get fondDePage(){
    return this._fondDePage;
  }

  set position(nouvellePosition){
    this._position = nouvellePosition;
  }

  set elements(nouvelElements){
    this._elements = nouvelElements;
  }

  set layout(nouveauLayout){
    this._layout = nouveauLayout;
  }

  set fondDePage(nouveauFond){
    this._fondDePage = nouveauFond;
  }

  ajouterElement(nouvelleElement){
    this.incrementPlans();
    this.elements.push(nouvelleElement);
  }

  /**
  * Renvoie l'élément ayant l'ID correspondant à l'ID en paramètre
  * @param {number} id de l'élément recherché
  * @returns {Element} l'élément ayant l'ID correspondant
  */
  getElement(id){
    return this.elements[id];
  }

  /**
  * Supprime l'élément ayant l'ID correspondant à l'ID en paramètre
  * @param {number} id de l'élément à supprimer
  */
  supprimerElement(id){
    this.elements.slice(id,1);
  }

  /**
  * Vide la page de tous ces éléments
  */
  vider(){
    this.elements = [];
  }

  /**
  * Ajoute +1 au plan des différents éléments présents sur la page
  */
  incrementPlans(){
    for (var i = 0; i < this._elements.length; i++) {
      this.elements[i].incrementPlanElem();
    }
  }

}
