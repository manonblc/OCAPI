/**
* Représente un album photo
* @class
*/
class AlbumPhoto {
  /**
  * Crée un album photo
  * @constructor
  * @param {string} titre Le titre de l'album
  * @param {number} nbPage Le nombre de pages initial de l'album
  * @param {taille} taille La taille de l'album, par défaut : 220mm x 290mm
  */
  constructor(titre,nbPage,taille = new Taille(290,220)) {
    // Vérification du nombre de pages (pair ou impair ?)
    if(nbPage%2 !== 0){
      nbPage++;
    }
    this.nbPages = nbPage;
    this.titre = titre;
    this.taille = taille;
    this.pages = [];
    for (var i = 1; i <= this.nbPages; i++) {
      this.pages.push(new Page(i));
    }

    this.zEvent = new Zevent();
    this.themeGeneral = null;
  }

  get NbPages(){
    return this._nbPages;
  }

  get Pages(){
    return this._pages;
  }

  set Pages(nouvellePages){
    this._pages = nouvellePages;
  }

  get titre(){
    return this._titre;
  }

  set titre(nouveauTitre){
    this._titre = nouveauTitre;
  }

  get zEvent(){
    return this._zEvent;
  }

  set zEvent(nouveauZEvent){
    this._zEvent = nouveauZEvent;
  }

  get taille(){
    return this._taille;
  }

  set taille(nouvelTaille){
    this._taille = nouvelTaille;
  }

  get themeGeneral(){
    return this._themeGeneral;
  }

  set themeGeneral(nouveauTheme){
    this._themeGeneral = nouveauTheme;
    this.appliquerTheme();
  }

  /**
  * Retourne la page présente à la position donnée
  *  ou null si la position n'est pas présente
  *
  * @param {number} position - La position de la page demandée
  * @return {Page} pages[position] - La page présent à position
  */
  pageAtPos(position){
    if(position <= this.nbPages && position >= 1){
      return this.pages[position-1];
    } else {
      return null;
    }
  }

  /**
  * Supprime la page présente à la position donnée
  *
  * @param {number} position - La position de la page demandée
  */
  supprimerPage(position){
    this.pages.slice(position,1);
    this.nbPages--;
    // Décrémentation des pages au dessus :
    for (var i = position; i < this.pages.length; i++) {
      this.pages[i].position--;
    }
  }

  /**
  * Supprime 2 pagse de l'album d'ID position et position+1
  * @param position - Position de la première page
  */
  supprimerDoublePage(position){
    this.supprimerPage(position);
    this.supprimerPage(position+1);
  }

  /**
  *  Crée 2 nouvelles pagse à la position donnée
  *   ou à la fin de l'album (nextPosition)
  *
  * @param {number} position - La position où créer les 2 nouvelles pages
  */
  newDoublePage(position = this.nextPosition){
    if(this.themeGeneral != null){
      var fonds = this.themeGeneral.randCoupleFond();
    }
    for (var i = 0; i < 2; i++) {
      this.newPage(position+i);
      if( !(fonds == null))
      this.pages[position+i].fondDePage = fonds[i];
    }
  }

  /**
  *  Crée 1 nouvelle page à la position donnée
  *
  * @param {number} position - La position où créer une nouvelle page
  */
  newPage(position){
    if(position < this.nextPosition){
      this.insertPage(position, new Page(position,this.taille));
    } else {
      this.pages[position] = new Page(position);
    }
    this.nbPages++;
  }

  /**
  * Récupère la position suivante pour rajouter une page à la fin de l'album
  * @return {number} nbPages + 1
  */
  get nextPosition(){
    return this.nbPages + 1;
  }

  /**
  *  Insère une page donnée à une position donnée
  *   Met également à jour l'attribut position de toutes les pages
  *
  * @param {number} position - La position d'insertion
  * @param {Page} page - La page à insérer
  */
  insertPage(position, page){
    this.pages.splice(position,0,page);
  }

  /**
  *  Crée un nouvel élément de type typeElem à la position posElem
  *    sur la page ayant la position posPage
  *
  * @param {number} posPage - La position de la page
  * @param {Position} posElem - La position de l'élement dans la page
  * @param {string} typeElem - Le type de l'élement [ "image" | "texte" ]
  * @return {number} 0,-1 ou -2 => 0 : Création réussie | -1 : position de page incorrecte | -2 : type élement incorrect
  */
  nouvelElementAPage(posPage, posElem, typeElem){
    var page = this.pageAtPos(posPage);
    if(page){
      var nouvelleElement;
      if(typeElem === "img"){
        nouvelleElement = new Photo(posElem);
      } else if(typeElem === "text"){
        nouvelleElement = new Texte(posElem);
      } else {
        // Erreur : type non reconnu
        return -2;
      }
      page.ajouterElement(nouvelleElement);
      // undo :
      this.zEvent.nouvelleAction(new Action("del","elem",true,posPage,[nouvelleElement,posElem]));
      return 0;
    } else {
      // Erreur : position de page non disponible
      return -1;
    }
  }

  /**
  * Ajoute un élément déjà existant à une page donnée
  * @param posPage - ID de la page
  * @param elem - Element à ajouter
  * @returns {number}
  */
  addElementAPage(posPage, elem){
    // undo :
    this.zEvent.nouvelleAction(new Action("del","elem",true,posPage,[elem,elem.position]));
    var page = this.pageAtPos(posPage);
    page.ajouterElement(elem);
    return 0;
  }

  /**
  * Applique le thème de l'album à l'album, utilisé par le setter de l'attribut themeGeneral
  */
  appliquerTheme(){
    if(this.themeGeneral!=null){
      for(let i = 0; i<this.nbPages;i+=2){
        let fonds = this.themeGeneral.fondCorrespondantAID(i);
        for (let j = 0; j < 2; j++) {
          // si c'est la deuxième page (juste après la couverture)
          if(i === 0 && j === 1){
            this.pages[i+j].fondDePage = "../Vue/data/images_png/Cadenas.png";
          }else{
            this.pages[i+j].fondDePage = fonds[j];
          }
        }
      }
    }
  }

  /**
  * Applique le thème de l'album à l'album, utilisé par le setter de l'attribut themeGeneral
  */
  appliquerThemeRandomAPage(id){
    if(id > 0 && id < nbPage && this.themeGeneral !== null)
    this.pages[id].fondDePage = themeGeneral.randCoupleFond();
  }

  /**
  * Applique la couleur donnée en paramètre à tous les textes de l'album
  * @param nouvelleCouleur
  */
  appliquerCouleurPolice(nouvelleCouleur){
    if(this.themeGeneral!=null){
      for(let i = 0; i<this.nbPages;i++){
        for(let j = 0; j<this.pages[i].elements.length; j++){
          if('chaine' in this.pages[i].elements[j]){
            this.pages[i].elements[j].couleurPolice = nouvelleCouleur;
          }
        }
      }
    }
  }
}
