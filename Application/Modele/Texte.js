/**
* Représente un texte
* @class
* @extends Element
*/
class Texte extends Element{
  /**
  * Crée un texte
  * @constructor
  * @param {Position} position - La position de l'élément
  * @param {number} taillePolice - La valeur de la taille de la police, défaut : 11,0
  */
  constructor(position,taillePolice = 11.0){
    super(position, new Taille(50, 220));
    this._chaine = null;
    this._font = null;
    this._taillePolice = taillePolice;
    this._couleurPolice="default";
    this._bold = false;
    this._italic = false;
    this._underline = false;
    this._align = 'left';
  }

  get chaine(){
    return this._chaine;
  }

  get font(){
    return this._font;
  }

  get taillePolice(){
    return this._taillePolice;
  }

  get couleurPolice(){
    return this._couleurPolice;
  }

  set couleurPolice(nouvelleCouleur){
    this._couleurPolice = nouvelleCouleur;
  }

  set chaine(nouvelleChaine){
    this._chaine = nouvelleChaine;
  }

  set font(nouvelleFont){
    this._font = nouvelleFont;
  }

  set taillePolice(nouvelleSize){
    this._taillePolice = nouvelleSize;
  }

  get bold() {
    return this._bold;
  }

  set bold(value) {
    this._bold = value;
  }

  get italic() {
    return this._italic;
  }

  set italic(value) {
    this._italic = value;
  }

  get underline() {
    return this._underline;
  }

  set underline(value) {
    this._underline = value;
  }

  get align() {
    return this._align;
  }

  set align(value) {
    this._align = value;
  }
}
