/**
* Représente une image
* @class
* @extends Element
*/
class Photo extends Element{
  constructor(position, source = ""){
    super(position, new Taille(100,290));
    this.source = source;
  }

  get source(){
    return this._source;
  }

  set source(nouvelleSource){
    this._source = nouvelleSource;
  }

}
