// Dimensions du canvas
d3.select('#vueGlobale')
.append('canvas')
.attr('width', window.innerWidth)
.attr('height', window.innerHeight);

const canvas = d3.select('canvas');
canvas.node().id = "bubulles";
const context = canvas.node().getContext('2d');
const width = canvas.property('width');
const height = canvas.property('height');
const centreX = width/2;
const centreY = height/2;

// Pour la gestion du zoom
var zoom = 0.7;

// Couleurs
const colorBulle = '#5dade2';
const colorLigne = '#C4D7ED';

// Effectue un rectangle pour le background
// d3.drag met à jour les valeurs
// centre de la page
const background = { x: 0, y: 0, x2: 0, y2: 0 };
// Diamètre des bulles
var radius = 100;

var tailleBulleIntermediaire = 30;
var tailleBulle = 100;

// Bulles et liens entre elles
var graph = [];



function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

var nbcol = 10;
var r = 40;
function getXBulle(i){

 let a;
  if(i == 1){
    a = 375;
  }else if(i%(nbcol*2)==0){
    a= 200;
  }else if(i%nbcol == 0){
    a = 100 + nbcol*200;
  }else{
    if (Math.floor(i / nbcol) % 2 == 0) {
        a = 150 + (i % nbcol) * 200;
    }else {
        a = 150 + nbcol*200 - (i % nbcol) * 200;
    }
  }
 return a+getRandomInt(20);


  //sinus
  //return 150 + i * 120;


  //spirale

  // return 100*i*Math.cos(i);

  //return i*50 + getRandomInt(50*i);
}

function getYBulle(i){
  let a;
  if(i === 0){
    a = 300;
  }else
  if(i % nbcol === 0){
    a =  150+ Math.floor(i/nbcol)*300;

  }else{
    a = 300+ Math.floor(i/nbcol)*300;
  }
  return a + getRandomInt(20);

  //sinus
   //return 500 - Math.sin(i) * 120;

   //spirale

  // return 100*i*Math.sin(i);


  //return i*50 + getRandomInt(50*i);

}


function creationGraph(){
  let nods = [];
  let lins = [];
  let i;
  for(i = 0; i<(album.nbPages); i+=2) {
    // Création de la bulle contenant 2 pages :
    nods[i] = {};
    nods[i].id = i + 1;
    // nods[i].x = 150 + i * 120;
    // nods[i].y = 500 - Math.sin(i) * 120;
    nods[i].x = getXBulle(i);
    nods[i].y = getYBulle(i);
    // Création de la bulle intermédiaire :
    if(i<album.nbPages-2){
      nods[i + 1] = {};
      nods[i + 1].id = i + 2;
      // nods[i + 1].x = 150 + (i + 1) * 120;
      // nods[i + 1].y = 500 - Math.sin(i + 1) * 120;
      nods[i + 1].x = getXBulle(i+1);
      nods[i + 1].y = getYBulle(i+1);
    }
  }

  // Création des liens entre les bulles :
  for(i = 0; i<nods.length-1; i++){
    lins[i]= {};
    lins[i].source = i;
    lins[i].target = i+1;
  }

  graph.nodes = nods;
  graph.links = lins;
}

creationGraph();

/**
* Ajoute 2 noeuds au graphe
*/
function ajoutPageGraph() {
  for(let i = 0; i < 2; i++){

    graph.nodes.push({id : graph.nodes.length+1,
      x :getXBulle(graph.nodes.length),
      y : getYBulle(graph.nodes.length)})

    graph.links.push({source : graph.nodes.length-2,
      target : graph.nodes.length-1});
  }
}

function ticked() {
  render()
}

render();

function getCible(x,y){
  let i = 0;
  let dx;
  let dy;
  let d2;
  let node;
  let cible = null;

  while(i < graph.nodes.length && cible===null){
    node = graph.nodes[i];

    dx = x - (node.x + background.x) * zoom;
    dy = y - (node.y + background.y) * zoom;
    d2 = dx * dx + dy * dy;

    if(node.id%2!==0){
      if(d2 < Math.pow(tailleBulle,2)){
        cible = node;
      }
    } else {
      if(d2 < Math.pow(tailleBulleIntermediaire,2)){
        cible = node;
      }
    }
    i = i + 1;
  }
  return cible;
}


/**
* Applique un décalage sur le background de la page principale en fonction de la position de la souris sur l'écran
* @param {number} x Position sur l'axe X de la souris lors du zoom
* @param {number} y Position sur l'axe X de la souris lors du zoom
*/
function decalageGraph(x,y){
  decalX = Math.abs(x - centreX)*(1/(zoom*10));
  decalY = Math.abs(y - centreY)*(1/(zoom*10));
  if(x > centreX){
    if(y < centreY){
      background.x += -decalX;
      background.y += decalY;
    } else {
      background.x += -decalX;
      background.y += -decalY;
    }
  } else {
    if(y < centreY){
      background.x += decalX;
      background.y += decalY;
    } else {
      background.x += decalX;
      background.y += -decalY;
    }
  }
}

/**
* Applique les transformations pour les effets de zoom
* @param e - L'event
* @returns {boolean}
*/
function gestionZoom(e){

  // Egalise l'objet événement
  let evt=window.event || e;
  // delta renvoie +120 quand la roue est déroulée vers le haut, -120 quand elle est déroulée vers le bas
  let delta=evt.detail? evt.detail*(-120) : evt.wheelDelta;
  if(delta > 0 && zoom < 2.5){
    zoom = zoom + 0.1;
    decalageGraph(e.clientX, e.clientY);
  } else if (zoom > 0.31) {
    zoom = zoom - 0.1;
    decalageGraph(e.clientX, e.clientY);
  }

  render();
  // désactive l'action par défaut de la molette de défilement de la page
  if (evt.preventDefault)
  evt.preventDefault();
  else
  return false;
}

var can = document.getElementsByTagName("canvas")[0];

canvas.on('click', () => {
  let coor = d3.mouse(canvas.node());
  let cible = getCible(coor[0],coor[1]);

  if(cible !== null){
    // Si la bulle n'est pas une bulle intermédiaire, alors on l'affiche
    if(cible.id%2!==0){
      pageAfficher = cible.id;
      afficherPages(pageAfficher);
      changeView();
    } else if (cible.id!==2) {
      // Sinon on ajoute une page
      // si ce n'est pas entre la couverture et la première page
      album.newDoublePage(cible.id-1);
      ajoutPageGraph();
      render();
    }
  }
});

// Pour que l'événement fonctionne avec Mozilla Firefox
var mousewheelevt=(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel"
// Si l'utilisateur utilise Internet explorer ou opera dans certain cas :
if (can.attachEvent)
can.attachEvent("on"+mousewheelevt, gestionZoom);
else if (can.addEventListener) // sinon si ce sont des navigateurs WC3
can.addEventListener(mousewheelevt, gestionZoom, false);

// Permet la gestion du déplacement par drag du background :
canvas.call(
  d3
  .drag()
  .subject(dragSubject)
  .on('start', dragStarted)
  .on('drag', dragged)
  .on('end', dragEnded)
  .on('start.render drag.render end.render', render)
)

/**
* Affiche les bulles, les liens entre chaque bulle et les miniatures des page dans le canva de la vue principale
*/
function render() {
  tailleBulle = 100 * zoom;
  tailleBulleIntermediaire = 30 * zoom;
  // Nettoyage du canva :
  context.clearRect(0, 0, width, height);
  //----------------------------------------------------------------------------------------------------------------------------------
  // Dessin des lignes
  context.strokeStyle = colorLigne //couleur des lignes
  context.lineWidth = Math.round(8 * zoom);
  context.beginPath()
  context.strokeStyle = colorLigne // couleur des lignes
  context.lineWidth = Math.round(8 * zoom);

  graph.links.forEach(link => {
    context.lineWidth = Math.round(8 * zoom);
    context.moveTo(
      (graph.nodes[link.source].x + background.x)*zoom,
      (graph.nodes[link.source].y + background.y)*zoom
    );
    if(link.source%(nbcol) == 0){
      context.quadraticCurveTo(
        (graph.nodes[link.source].x + background.x)*zoom,
        (graph.nodes[link.target].y + background.y)*zoom,
        (graph.nodes[link.target].x + background.x)*zoom,
        (graph.nodes[link.target].y + background.y)*zoom
      );
    }else if(link.source%(nbcol) == nbcol-1 ){
      context.quadraticCurveTo(
        (graph.nodes[link.target].x + background.x)*zoom,
        (graph.nodes[link.source].y + background.y)*zoom,
        (graph.nodes[link.target].x + background.x)*zoom,
        (graph.nodes[link.target].y + background.y)*zoom
      );
    }else{
    context.lineTo(
      (graph.nodes[link.target].x + background.x)*zoom,
      (graph.nodes[link.target].y + background.y)*zoom
    );
  }
  });

  context.stroke();
  //----------------------------------------------------------------------------------------------------------------------------------
  // Dessin des bulles :
  context.beginPath();
  context.lineWidth = 0;

  graph.nodes.forEach(node => {
    context.moveTo((node.x + background.x + (node.id%2===0?tailleBulleIntermediaire:tailleBulle))*zoom, (node.y + background.y)*zoom);
    context.arc(
      (node.x + background.x)*zoom,
      (node.y + background.y)*zoom,
      (node.id%2===0?tailleBulleIntermediaire:tailleBulle),
      0,
      2 * Math.PI
    );
  });

  context.fillStyle = colorBulle; // couleur des bulles
  context.fill();
  //----------------------------------------------------------------------------------------------------------------------------------
  // Dessins des miniatures des pages :
  // Fonction de dessin des éléments :
  function dessinImage(ctx,l,h,x,y,rX,rY,img){


    //Calcule de la taille de la partie de l'élément présente dans sur la page
    let largeur = img.taille.largeur*ratioRetourX*img.echelleX*rX;
    let hauteur = img.taille.longueur*ratioRetourY*img.echelleY*rY;

    let iX = x+(img.position.posX*rX);
    let iY = y+(img.position.posY*rY);

    let sX = 0;
    let sY = 0;

    let swidth = img.taille.largeur;
    let sheight = img.taille.longueur;

    //Calcule position de l'image à déssiner par rapport à la vue globale
    if((img.position.posX)<0){
      sX = -(img.position.posX*(1/ratioRetourX)*(1/img.echelleX));
      swidth = swidth - sX;
      largeur = largeur - (x-iX);
      iX = x;
    }

    if((img.position.posY*r)<0){
      sY = -(img.position.posY*(1/ratioRetourY)*(1/img.echelleY));
      sheight = sheight - sY;
      hauteur = hauteur - (y-iY);
      iY = y;
    }

    if((iX+largeur)>(x+l)){
      swidth = (swidth*((l-(iX-x))/largeur));
      largeur = l-(iX-x);
    }

    if((iY+hauteur)>(y+h)){
      sheight = (sheight*((h-(iY-y))/hauteur));
      hauteur = h-(iY-y);
    }
    // Si l'image a une source, alors on l'affiche
    if (img.source !== ""){
      let image = new Image();
      image.src = img.source;

      //ctx.drawImage(image, x+(img.position.posX*r),y+(img.position.posY*r),largeur,hauteur);
      ctx.drawImage(image,sX,sY,swidth,sheight,iX,iY,largeur,hauteur);

    } else {
      // Sinon on affiche un carré à la place de l'image
      ctx.beginPath();
      ctx.lineWidth = "1";
      ctx.strokeStyle = "black";
      ctx.rect(x+(img.position.posX*rX),y+(img.position.posY*rY), largeur, hauteur);
      ctx.stroke();
    }
  }

  function dessinTexte(ctx,l,h,x,y,rX,rY,texte){
    ctx.beginPath();

    ctx.lineWidth = 1;
    ctx.font = Math.round(11*rX).toString()+"px Arial";
    if(texte.couleurPolice !== "default"){
      ctx.strokeStyle = texte.couleurPolice;
    }else{
      ctx.strokeStyle = "black";
    }
    ctx.strokeText(texte.chaine,x+(texte.position.posX*rX),y+(texte.position.posY*rY));
  }

  function dessinElementPage(ctx,l,h,x,y,rX,rY,listeElem){
    for (let i  = 0 ; i < listeElem.length ; i++) {
      if('chaine' in listeElem[i]){
        dessinTexte(ctx,l,h,x,y,rX,rY,listeElem[i]);
      } else {
        dessinImage(ctx,l,h,x,y,rX,rY,listeElem[i]);
      }
    }

    // On réinitialise la largeur des lignes et leur couleur pour afficher le chemin :
    ctx.lineWidth = "5";
    ctx.strokeStyle = colorLigne; // Couleur des lignes
  }

  function afficherCouverture(node){
    context.beginPath();
    context.lineWidth = "0";
    // Diamètre du cercle au carré :
    let d = Math.pow((tailleBulle*0.90)*2,2);
    // Ratio de l'album :
    let ratio = (idealWidth>idealHeight ? idealHeight/idealWidth : idealWidth/idealHeight);
    // Transformation nécessaire du ratio pour trouver la position des points du rectangle [avec l/L = ratio] inscrit dans le cercle
    let calcRatio = (1/(1+(Math.pow(ratio,2))));
    // Position de la couverture :
    let p1X = ((node.x + background.x)*zoom) - (0.5) * (Math.sqrt(d*calcRatio)*ratio); // - 2 Pour la marge entre les 2 pages
    let p1Y = ((node.y + background.y)*zoom) - (0.5) * Math.sqrt(d*calcRatio);
    // Calcul de la taille des pages :
    let largeur = (Math.sqrt(d*calcRatio)*ratio);
    let hauteur = Math.sqrt(d*calcRatio);
    // Calcul du ratio permettant de calculer la taille des éléments dans la page :
    let rElemX = (largeur/(idealWidth));
    let rElemY = (hauteur/(idealHeight));

    // Remplissage des pages en blanc :
    context.fillStyle = 'white';
    // Affichage de la couverture
    if(album.pageAtPos(node.id).fondDePage != null){
      ima = new Image();
      ima.src = album.pageAtPos(node.id).fondDePage;
      context.drawImage(ima, p1X, p1Y, largeur, hauteur);
    }else{
      context.fillRect(p1X,p1Y,largeur,hauteur);
    }

    if (album !== null){
      if(album.pageAtPos(node.id)!==null){
        // Affichage des éléments de la couverture
        dessinElementPage(context,largeur,hauteur,p1X,p1Y,rElemX,rElemY,album.pageAtPos(node.id).elements);
      }
    }
  }

  function afficherPage(node){
    context.beginPath();
    context.lineWidth = "0";
    // Diamètre du cercle au carré :
    let d = Math.pow((tailleBulle*0.90)*2,2);
    // Ratio de l'album :
    let ratio = (idealWidth>idealHeight ? idealWidth/idealHeight : idealHeight/idealWidth);
    // Transformation nécessaire du ratio pour trouver la position des points du rectangle [avec l/L = ratio] inscrit dans le cercle
    let calcRatio = (1/(1+(Math.pow(ratio,2))));
    // Position de la page de gauche :
    let p1X = ((node.x + background.x)*zoom) - (0.5) * (Math.sqrt(d*calcRatio)*ratio) - 2; // - 2 Pour la marge entre les 2 page
    let p1Y = ((node.y + background.y)*zoom) - (0.5) * Math.sqrt(d*calcRatio);
    // Position de la page de droite :
    let p2X = ((node.x + background.x)*zoom) + 2; // + 2 pour la marge entre les 2 pages
    let p2Y = ((node.y + background.y)*zoom) - (0.5) * Math.sqrt(d*calcRatio);
    // Calcul de la taille des pages :
    let largeur = (0.5)*(Math.sqrt(d*calcRatio)*ratio);
    let hauteur = Math.sqrt(d*calcRatio);
    // Calcul du ratio permettant de calculer la taille des éléments dans la page :
    let rElemX = (largeur/(idealWidth));
    let rElemY = (hauteur/(idealHeight));

    // Remplissage des pages en blanc :
    context.fillStyle = 'white';

    if(album.pageAtPos(node.id-1).fondDePage != null){
      ima = new Image();
      ima.src = album.pageAtPos(node.id-1).fondDePage;
      context.drawImage(ima,p1X, p1Y, largeur, hauteur);
    }else{
      context.fillRect(p1X,p1Y,largeur,hauteur);
    }
    if(album.pageAtPos(node.id).fondDePage != null){
      ima = new Image();
      ima.src = album.pageAtPos(node.id).fondDePage;
      context.drawImage(ima,p2X, p2Y, largeur, hauteur);
    }else{
      context.fillRect(p2X,p2Y,largeur,hauteur);
    }

    // Ces 2 conditions permettent d'éviter des erreurs liées au temps de chargement des fichiers js
    // (le modèle ne fonctionne pas totalement correctement pendant ce laps de temps)
    if (album !== null){
      if(album.pageAtPos(node.id)!==null){
        if(node.id !==3){
          // Affichage des éléments de la page de gauche
          dessinElementPage(context,largeur,hauteur,p1X,p1Y,rElemX,rElemY,album.pageAtPos(node.id-1).elements);
        }
        // Affichage des éléments de la page de droite
        dessinElementPage(context,largeur,hauteur,p2X,p2Y,rElemX,rElemY,album.pageAtPos(node.id).elements);
      }
    }

    //ajout des numéros des pages
    context.fillStyle="black";
    context.font = Math.round(35*zoom).toString()+"px Arial";
    let s;
    if(node.id === 3){
      s = "1";
    }else if (node.id >3){
      s = (node.id-3).toString()+"-"+(node.id-2).toString();
    }

    if(node.id === 3){
      context.fillText(s, p2X,p2Y+140*zoom);
    }else if(node.id <= 12){
      context.fillText(s, p2X-25*zoom,p2Y+140*zoom);
    }else{
      context.fillText(s, p2X-50*zoom,p2Y+140*zoom);

    }

  }

  graph.nodes.forEach(node => {
    // Dessin des carrés représentant les pages dans les bulles :
    if(node.id%2!==0){
      if(node.id === 1){
        afficherCouverture(node);
      } else {
        afficherPage(node);
      }
    }else{
      if(node.id !== 2){
        context.beginPath();
        context.lineWidth = "0";
        // Diamètre du carré inscrit dans le cercle
        let d = Math.pow((tailleBulleIntermediaire*0.95)*2,2);
        // Position du carré inscrit
        let p1X = ((node.x + background.x)*zoom) - (0.5) * (Math.sqrt(d*(1/2)));
        let p1Y = ((node.y + background.y)*zoom) - (0.5) * Math.sqrt(d*(1/2));
        // Dimensions du carré
        let dim = Math.sqrt(d*(1/2));
        // Rectangle horizontal
        context.rect(p1X+2/5*dim,p1Y,1/5*dim,dim);
        // Rectangle vertical
        context.rect(p1X,p1Y+2/5*dim,dim,1/5*dim);
        // Remplissage
        context.fillStyle = 'white';
        context.fill();
      }
    }
  });

}
//----------------------------------------------------------------------------------------------------------------------------------
function dragSubject() {
  let i
  let n = graph.nodes.length
  let dx
  let dy
  let d2
  let node
  let subject

  for (i = 0; i < n; i += 1) {
    node = graph.nodes[i]
    dx = d3.event.x - (node.x - background.x) * zoom
    dy = d3.event.y - (node.y - background.y) * zoom
    d2 = dx * dx + dy * dy;
    // Partie pour drag and drop le background
    if (typeof subject === 'undefined') {
      subject = background;
    }
  }
  return subject
}

// var decX;
// var decY;

function dragStarted() {
  d3.event.subject.active = true
  // decX = d3.event.x*(1-zoom);
  // decY = d3.event.y*(1-zoom);
  // console.log(decX);
  // console.log(decY);

}

function dragged() {
  d3.event.subject.x = d3.event.x;
  d3.event.subject.y = d3.event.y;
  // d3.event.subject.x = d3.event.x*(1/zoom) - decX;
  // d3.event.subject.y = d3.event.y*(1/zoom) - decY;
  // console.log(d3.event.x*(1/zoom) - decX);
  // console.log(d3.event.y*(1/zoom) - decY);

}

function dragEnded() {
  d3.event.subject.active = false;
}
