/**
* Représente une action effectuée sur l'album
*/
class Action {
  /**
  * Crée une nouvelle action
  * @param {string} type - Le type de l'action ex :"modif"
  * @param objet - L'objet qui a subi la transformation
  * @param {boolean} exist - Si l'objet en question existe ou s'il vient d'être supprimé
  * @param {number} idPage - L'ID de la page
  * @param complements - Un tableau contenant des informations complémentaires
  */
  constructor(type,objet,exist,idPage,complements = null) {
    this.typeAct = type;
    this.objet = objet;
    this.exist = exist;
    this.idPage = idPage;
    this.complements = complements;
  }

  get typeAct(){
    return this._typeAct;
  }

  get objet(){
    return this._objet;
  }

  get exist(){
    return this._exist;
  }

  get idPage(){
    return this._idPage;
  }
  
  get complements(){
    return this._complements;
  }

  set typeAct(nouvelTypeAct){
    this._typeAct = nouvelTypeAct;
  }

  set objet(nouvelObj){
    this._objet = nouvelObj;
  }

  set exist(existance){
    this._exist = existance;
  }

  set idPage(nouvelIdPage){
    this._idPage = nouvelIdPage;
  }

  set complements(nouveauComplement){
    this._complements = nouveauComplement;
  }

}
