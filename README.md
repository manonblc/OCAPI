# OCAPI
Notre produit OCAPI, ou Outil de Création d'Album Photos Interactif, est une application web qui permet la réalisation d'albums photos et qui offre de nombreuses possibilités en terme de personnalisation.


## Bien débuter
Les instructions suivantes indiquent la démarche à suivre afin de faire tourner notre produit localement et ce, sur n'importe quelle machine.

De plus une vidéo présentant le fonctionnement idéal de OCAPI est fourni dans DossierProjet.
### Prérequis
Il est nécessaire d'être connecté à Internet afin de profiter de l'ensemble des fonctionnalités de notre outil.

### Installation
Il faut avoir extrait le contenu du fichier OCAPI.zip puis, il faut ouvrir le fichier OCAPI/Application/Vue/vueGenerale.html dans un navigateur afin d'arriver sur la vue d'accueil de notre outil.

Vous êtes enfin prêts à utiliser OCAPI !

Nous sommes conscient que certains bugs peuvent avoir lieu, nous fournissons donc une liste de bugs connus dans Application/Liste_De_Bugs.txt .

## Codé avec
* HTML5 - Langage de balisage servant à représenter des pages web
* CSS3 - Langage servant à décrire la présentation de nos documents HTML
* JavaScript - Langage servant à créer des pages web interactives
* fabric.js - Librairie de développement web
* D3.js - Librairie de développement web

## Accéder à notre documentation
Se référer au dossier Documentation contenant un fichier Documentation_OCAPI.pdf et un site généré aves JsDoc.

## Auteurs du projet
* **Rémi Barra** - *Responsable de la rédaction et de l'analyse de l'existant*
* **Manon Blanco** - *Responsable de la conception*
* **Antoine Blanquet** - *Chef de projet*
* **Mathias Cabezas** - *Responsable IHM*
* **Hugo Prat-Capilla** - *Responsable de la programmation*
* **Manuel Sulmont** - *Responsable des risques et des contraintes*
* **Camille Vacher** - *Responsable des tests*
